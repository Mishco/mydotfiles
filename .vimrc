" 
" Michal Slovik .vimrc
" 

" set leader key to space
let mapleader = "\<Space>"

set title
set titlestring="%t %{expand(\"%:p:h\")}"

" colors and syntax
set number
syntax on
set showmode
set showcmd
set showmatch
filetype indent on	


" cleaner stuff with files
set nobackup
set nowritebackup
set noswapfile

" improve search
set incsearch
set hlsearch
set ignorecase

set spell
set visualbell

set autoindent
set cindent
set smartindent
set smarttab

" move to begging/end of line
nnoremap B ^
nnoremap E $

" $/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>

