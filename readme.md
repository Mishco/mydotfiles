# My Dot Files

Dotfiles are the customization files that are used to personalize your Linux or another Unix based system.
This repository contains my personal dotfiles. 

## How to manage your own dotfiles

There are a hundred ways to manage your dotfiles. 
My first suggestion would be to read up on the subject. 
A great place to start is "Your unofficial guide to dotfiles on GitHub": https://dotfiles.github.io/

Personally, I use the git bare repository method for managing my dotfiles: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/


## Unix

### Install vim config on unix machine

1. clone this repo

2. go the folder `cd mydotfiles/` 

3. copy `.vimrc` into home folder

```bash
cp .vimrc ~/.vimrc
```


## Windows 10

### Nvim

Folder for nvim configuration 

``` 
C:\Users\<USERNAME>\AppData\Local\nvim\init.vim
```


## Sources

* https://bitbucket.org/durdn/cfg/src/master/

## Manjaro 

### Nvim

Location of config file for nvim. For more information please visit [Neovim wiki](https://wiki.archlinux.org/title/Neovim)

```bash
~/.config/nvim/init.vim
```

## Symbolic links

Run `makesymlinks.sh` to create symbolic links
