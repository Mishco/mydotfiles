set number	    " show line number
syntax on
set nobackup
set relativenumber 	
set nowritebackup
set noswapfile
set showmode
set showcmd	    " show command in bottom line 

filetype indent on
set autoindent
set wrap

set hlsearch
set showmatch
set incsearch
set ignorecase

set spell	
set visualbell	
 
set autoindent	
set cindent	
set shiftwidth=4	
set smartindent	
set smarttab	
set softtabstop=4	
 
" Yaml file handling
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
filetype plugin indent on
autocmd FileType yaml setl indentkeys-=<:>

" Copy paste with ctr+c, ctr+v, etc
:behave mswin
:set clipboard=unnamedplus
:smap <Del> <C-g>"_d
:smap <C-c> <C-g>y
:smap <C-x> <C-g>x
:imap <C-v> <Esc>pi
:smap <C-v> <C-g>p
:smap <Tab> <C-g>1> 
:smap <S-Tab> <C-g>1<


" Go to start/end of line 
:smap <C-e> $
:smap <C-a> ^

" $/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>

" Note, the above line is ignored in Neovim 0.1.5 above, use this line instead.
set termguicolors

colorscheme darkblue

